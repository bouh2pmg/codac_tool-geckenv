FROM ubuntu

MAINTAINER Hugo MARTIN<hugo.martin@epitech.eu>

RUN \
	sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list && \
	apt-get update -y && \
	DEBIAN_FRONTEND=noninteractive apt-get install -y mysql-server

RUN \
	apt-get upgrade -y && \
	apt-get install build-essential software-properties-common byobu curl git htop man unzip vim nano wget -y

RUN \
	mkdir -p /var/run/mysqld && \
	chown mysql:mysql /var/run/mysqld && \
	sed -i 's/^\(bind-address\s.*\)/# \1/' /etc/mysql/my.cnf && \
	sed -i 's/^\(log_error\s.*\)/# \1/' /etc/mysql/my.cnf && \
	echo "mysqld_safe &" > /tmp/config && \
	echo "mysqladmin --silent --wait=30 ping || exit 1" >> /tmp/config && \
	echo "mysql -e \"grant all privileges on *.* to root@localhost identified by 'root';\"" >> /tmp/config && \
	cat /tmp/config && \
	bash /tmp/config
COPY databases /tmp/dbs
ENV dbsDir=/tmp/dbs

RUN curl -sL https://deb.nodesource.com/setup_11.x | bash - && apt-get install -y nodejs

#PHP
RUN \
	DEBIAN_FRONTEND=noninteractive apt-get install -y php libapache2-mod-php php-mysql mysql-common && \
	rm -rf /var/lib/apt/lists/*	\
	rm -f /tmp/config

RUN rm -f /var/www/html/index.html
RUN phpenmod mysqlnd && phpenmod pdo && phpenmod pdo_mysql

#PYTHON
RUN \
	apt-get install -y python3 && \
	rm -rf /var/lib/apt/lists/*	\
	rm -f /tmp/config

ENV HOME /root

#Scripts
COPY students.txt ${HOME}/
COPY scripts ${HOME}/.scripts
ENV PATH=$PATH:${HOME}/.scripts

EXPOSE 3306
EXPOSE 80

WORKDIR /root

#SSH
COPY .ssh ${HOME}/.ssh

ENTRYPOINT eval "$(ssh-agent -s)" && ssh-add ${HOME}/.ssh/geckenv && \
	service mysql start && service apache2 start && /bin/bash
