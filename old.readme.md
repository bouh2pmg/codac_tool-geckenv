# GeckEnv
### Docker image to mark students

#### Installation
First of all, please login with your gitlab account :
```bash
docker login registry.gitlab.com
```

Now you are able to pull the image :
```bash
docker pull registry.gitlab.com/geckos/geckenv
```
That's all !

#### Usage
##### Internal tests
```bash
docker run --rm -ti registry.gitlab.com/geckos/geckenv bash
```
##### Apache tests
```bash
docker run --rm -ti -p 8080:80 registry.gitlab.com/geckos/geckenv bash
```
> Then go to http://localhost:8080

#### List of available features :
1. C & GCC
2. PYTHON
3. PHP CLI
	PHP 7
4. PHP APACHE
	Please use this path : 
		> /var/www/html
		
5. CLONE
	An interesting feature is the clone command
	This command'll clone all students repositorys from geckos access.
	How to use it ?
	> clone [repository] 
	
6. LOAD
	Load will import for you a database in mysql. You can add some by adding some .sql in databases's directory.
	You can list all available databases :
	> load -l
	
	By this way, you can import a database :
	> load C_Go-PHP_D02_base-test